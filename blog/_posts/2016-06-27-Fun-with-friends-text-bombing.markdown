---

layout: blog
title:  Fun with friends - Text bombing
author: ThatGeoGuy
description: A fun waste of time that is sure to make your friends hate you
tags:
- Code
- Programming
- Android
- Fun
- Trolling
excerpt: This is something I discovered recently that may be a bit of an... anti-feature. The gist of it is, by using some basic system automation, I managed to automate my phone such that I could endlessly text my friends. Like, anything I wanted, and for pretty much forever. It was surprising to me that 1) Android exposes this behaviour, but mostly 2) that it was so simple to set up.

---
# {{ page.title }}
<p class='post-meta'>On <em>{{ page.date | date: "%Y-%m-%d" }}</em> by <em>{{ page.author }}</em></p>

This is something I discovered recently that may be a bit of an...
anti-feature. The gist of it is, by using some basic system automation, I
managed to automate my phone such that I could endlessly text my friends. Like,
anything I wanted, and for pretty much forever. It was surprising to me that 1)
Android exposes this behaviour, but mostly 2) that it was so simple to set up.

Anyways, perhaps I should explain how this all started. One day I got in a bit
of a silly argument with a friend, and I jokingly threatened that I would send
them text messages for the next 24 hours. I confidently proclaimed that my
friend would get no sleep, and they would regret having such a silly argument
with me. I was duly reminded that at some point I would have to sleep, so my
plain was unfeasible and therefore would never work. After an hour or so of
experimenting, I came up with a clever script to send text messages in even
intervals for the remainder of the night, and subsequently relished in the fact
that I was the winner of a petty bet.

<p class='post-meta'>
Disclaimer: depending on where you live, and what your
local spam laws are like, this could either be really illegal or could go
ignored. In any case, make sure you're only doing this to people who know you,
and from a number they can reach you at. Don't do this to harass people: going
out of your way to spam or harass someone outside of a shared joke (note: both
sides need to share the joke) is a dick move. Nonetheless, I'm not going to be
held responsible if you do something stupid with this.
</p>

## The fun begins! What you'll need:

Of course, the fun here can only begin once we've procured the necessary
materials. Sure enough, if you have an Android phone and a plan that can send
SMS messages, you're 90% of the way there. The last thing you'll need is the
Tasker App, available through [Google
Play](https://play.google.com/store/apps/details?id=net.dinglisch.android.taskerm)
or through the developer's [website](http://tasker.dinglisch.net/). Before
going on, I should at least explain what Tasker is and why we need it.

Tasker is an app for Android phones that more or less allows one to automate
activities. Usually, one would automate actions based on some context, such as
*time of day*, *battery level*, *location*, or even *what's plugged into your
phone*. Of course, Tasker allows you to access the full facilities of the
phone, which means you can do anything like send an emergency SMS, place a
phone call, or even start another app. To be honest, I used to find Tasker much
more useful than I do today, especially with how limited the Android operating
system was back in the day (think, Android 1.5 days). In any case, the point is
that we can create some pretty useful things with Tasker, as well as some...
less useful ones. Nonetheless, in the spirit of hacking, let's start!

## Creating a task

I don't think it's worth giving a full blown tutorial of Tasker here, so I'll
just assume you know how to find the **Tasks** tab in the app and can hit the
plus (+) symbol at the bottom of the screen. We're gonna create a new task,
which you can call whatever you want. For this task, I decided to call it [Poo
in Loo](https://www.youtube.com/watch?v=_peUxE_BKcU), for reasons that will
soon become obvious.

In any case, the script itself is pretty easy to set up. We want three things:

1. A tag and goto statement to act as a loop so our code keeps running.
2. A task to actually send the message itself.
3. A timer so that we don't send a few hundred text messages a second. This
   could be seriously bad. In some places such a volume of messages would fall
   under spam laws, but even innocently this could get you kicked off your plan
   with your carrier. Further, your recipient surely does not want all these
   texts, so try to keep it tame.

Once the script is setup, it should look roughly as follows, with the text
message task looking like what is seen on the left:

<img style='max-width: 800px; width: 100%; border: solid 2px black;'
     src='{{ site.baseurl }}/img/text-bombing-setup.png' />

And voila! You can now unleash this beast by pressing the play button in the
bottom left corner. Note that it will keep going forever, unless the Android OS
kills Tasker off (only happens if you set Tasker to run as a background
process). That said, be sure to stop the madness eventually, or you won't have
many friends left over.

## Conclusion

In short, this was a pretty fun experiment, at least while it lasted. I don't
imagine too many uses of this hack, but it is an interesting way to get back at
your friends, or at least to get them to turn off their phones. Despite this,
variations of the same technique could be useful for other things as well. For
example, since many of my colleagues and I do not work in the same office, I
have toyed with the idea of setting up a task like this on a given schedule, to
send a message when a few of us go for lunch, so the others know to follow
suit. At the end of the day, it was a fun, short hack I came up with to win a
bet that probably didn't matter anyways. If you have your own variation of
this, or if you use Tasker for something else, I'd love to hear about it! Leave
a comment below and I'll check it out.

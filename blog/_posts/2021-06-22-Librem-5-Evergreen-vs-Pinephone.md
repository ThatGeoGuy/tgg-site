---

layout: blog
title: Librem 5 Evergreen vs. Pinephone (Part 2 of ???)
author: ThatGeoGuy
description:
tags:
- FOSS
- Purism
- Librem 5
- Pine64
- Pinephone
- smartphones
- technology
excerpt: |-
    It's been about half a year since I wrote my [original
    post](/blog/2020/12/06/Librem-5-Evergreen-vs-Pinephone/) comparing the Librem 5 and Pinephone.
    The original post saw some controversy as well as quite a bit of attention on [Hacker
    News](//news.ycombinator.com/item?id=25338633). Surprisingly, for a market dominated by new tech
    every year, there remains quite a bit of interest in these two devices.

    Development on both devices continues day-by-day, in small and large parts. I wanted to revisit
    the devices as a lot of the ecosystem has changed. Further, I think they've definitely both
    evolved in terms of what's possible, and more importantly what's easy.

---
# {{ page.title }}
<p class='post-meta'>On <em>{{ page.date | date: "%Y-%m-%d" }}</em> by <em>{{ page.author }}</em></p>

It's been about half a year since I wrote my [original
post]({{ site.baseurl }}/blog/2020/12/06/Librem-5-Evergreen-vs-Pinephone/) comparing the Librem 5 and
Pinephone. The original post saw some controversy as well as quite a bit of attention on [Hacker
News](//news.ycombinator.com/item?id=25338633). Surprisingly, for a market dominated by new
tech every year, there remains quite a bit of interest in these two devices.

Development on both devices continues day-by-day, in small and large parts. I wanted to
revisit the devices as a lot of the ecosystem has changed. Further, I think they've definitely both
evolved in terms of what's possible, and more importantly what's easy.

So with that said, let's get into it.

# What's actually changed?

Before being able to actually talk about the progress, I have to give the same disclaimer that I
gave in the original article: I'm not testing voice, SMS, or MMS here. As far as I've heard from
other sources, calls and SMS _seem_ to be in working order. MMSd has (from what I've gathered, at
least) been a large focus for many projects in the last six months. I try my best not to use MMS
whenever possible, so I'm certainly a poor source of information if that's a blocker for you to
adopt one of these two devices.

## The phones

### Purism Librem 5

Between the two phones, the Librem 5 has definitely surprised me the most in terms of actual
development over the last half-year. This has been for both good and bad reasons, but overall I've
definitely delved into the phone a lot more than I had the opportunity to last
December.[^december-usage]

### Pinephone

The Pinephone has been pretty stable for the most part, and hasn't really changed. I am looking
forward to some of the custom back cases for the phone itself, in particular the wireless charging
case. It seems quite exciting to me that the hardware can continue to be hacked and improved. I
don't care much for a keyboard case with the Pinephone, but I am very excited to see how the
wireless charging case performs. Wireless charging is something that I definitely miss with both the
Pinephone and Librem 5.

I didn't really have another spot to talk about this, but Ubuntu Touch has of late been maintaining
a more up-to-date kernel for the Pinephone on their `kernelupgrade` channel. If you're planning to
give Ubuntu Touch a try, do not be afraid to use this channel. Back in January it was much more
unstable, but I think now it is almost better than the official release on `stable`. There is
probably some more work involved in getting it ready for stable, and I cannot comment on that here
as I am not an Ubuntu Touch developer. However, my day to day experience with this channel in Ubuntu
Touch was much vastly improved thanks to much of the hardware being more responsive. Some notable
things on this kernel that do not really work on stable right now:

* Sound switches immediately to headphones when you plug in headphones. On `stable`, you can plug in
  headphones but the kernel fails to recognize that you've done anything.
* The kill switch for the modem can be used. On `stable` if you turn the kill-switch to off
  position (i.e. kill the modem) and you disconnect from WiFi because the phone went into sleep
  mode, sometimes your WiFi and Bluetooth never come back.
* The screen runs at 60FPS, whereas on `stable` I believe it is locked to 30FPS. It makes a huge
  difference!

## The Ecosystem

### Phosh / Lomiri / Plasma

No surprises here, I still vastly prefer Lomiri to Phosh. However, Phosh did get some nice-to-have
upgrades. Auto rotation now works, which is very nice. Battery percentages in the Phosh drop-down
menu from the top of the display also now correctly reports battery! These were minor issues in
Phosh but it seems that since it is becoming more and more of a default for many projects outside
PureOS that it is receiving consistent love.

Unfortunately, Phosh still isn't very fluid, and suffers from many of the same downfalls I
complained about last time. I don't have a lot to say here, but I can definitely say that there's a
lot of room to grow.

I did get the opportunity to try out KDE Plasma via Manjaro on my Pinephone. The overall feel was
much closer to older versions of Android (in particular, Android 4 and 5). Many of the gestures and
interactions were much more fluid than say, Phosh, which doesn't have gestures or animations to pair
with those gestures. However, this is definitely the weakest UI / OS / environment out of the bunch.
A few reasons in no particular order:

* The screen timeout is too dang short! Seriously, I counted 15 seconds, and the screen starts to
  dim. Further, there's no setting in the *Settings* app to actually control this. It seems like
  [there's workarounds](//forum.pine64.org/showthread.php?tid=13481), but I wasn't really
  impressed enough to stick around on Plasma for now.
* The sofware store isn't nearly as polished as the OpenStore on Ubuntu Touch nor the Software
  Centre in GNOME. I think if I were to rank them I'd go OpenStore > GNOME Software > Command line >
  KDE Plasma. I had a difficult time being able to open it and install apps and updates. It was much
  slower, and overall you can tell it had a lot less time put into it. As far as design goes, it was
  quite nice! I do like OpenStore better, but mostly it just felt buggy on the Pinephone. Plus, and
  I may be mistaken here, but there didn't seem to be a way to get flatpak integrated into
  it?[^openstore]
* Default apps and app selection seemed much more limited. Obviously you can install whatever you
  want from the terminal (at your own ~~peril~~ ~~risk~~ behest), but doing so on a tiny phone
  screen is remains cumbersome.
* There is definitely a bit of a visual distraction with regards to mixing GTK and Qt / KDE apps. It
  is quite wild to see such different visual experiences next to one another, and I think the
  experience overall suffers for it. Not to say that GTK is the way forward, but given that many of
  the most usable apps today were built in GTK or oriented towards Phosh somehow mean that the KDE
  experience is drawn back by that.

Overall, I felt like my experience with KDE Plasma was pretty bad. I hope to continue to keep
testing it as it seems to be the default for new Pinephone units going forward. I am confident it
will continue to get better over time, but my initial experience was definitely a step down.

### Music

I believe last time I complained that neither the Pinephone nor Librem had Bandcamp or Spotify apps
available. This is still true.[^bandcamp] However, shortly after writing my previous review of the
phones, quite a lot of progress on the sound front came about. For starters, an update was pushed
quite rapidly to address the fact that microSD cards were not being automatically mounted. This
makes the experience of just throwing all your music on a high-capacity microSD card more or less
ideal.

Between running [Lollypop](//wiki.gnome.org/Apps/Lollypop) on the Librem 5 and running the default
[Ubuntu touch music app](//wiki.ubuntu.com/Touch/CoreApps/Music/Design) or the default [music app in
KDE Plasma](//apps.kde.org/elisa/), Lollypop is by far the best out of all of them. I did bring up
Lollypop before, but my experience so far is that the Librem 5 (using Lollypop, not sure if this
varies based on software) is much, much better than that of the Pinephone.

To be fair here, the Librem 5 does actually include quite a [fancy
DAC](//www.cirrus.com/products/wm8962-62b/) in comparison to the Pinephone. When wired in, it does
sound better. However, mainly I think the difference I've seen is that it manages to play FLAC and
other lossless or high-fidelity formats without skips or interruptions. In contrast, the Pinephone
does sometimes struggle with playback. Its music app should be able to run FLAC flawlessly, as I
have definitely gotten FLAC to work. However, the music app does have some problems. Certain FLAC
tracks for whatever reason seem to be unplayable, and I honestly can't tell why. Other times, the
app will skip like CDs used to, and you'll miss ~1-2 seconds of a song.  Lastly, the app really
struggles when trying to shuffle through big collections on a microSD card. I have ~60GiB of music
on a microSD, and occasionally the music app will just die when trying to go to the next song.

Overall, Lollypop on the Librem 5 is the clear winner. I took a long drive to Tulsa earlier this
year (~10 hours), and I chose to bring the Librem 5 with me to play music during the drive. I had to
keep it plugged into an external battery pack, but overall the sound quality and performance of the
app itself were what you would hope for.

### Flatpak

One thing I discovered which changed a lot of my experience with Phosh (Librem 5 / PureOS and any
Phosh-based distro for the Pinephone) was that flatpak was so easy to install and set up. For PureOS
or any Debian-based distro I believe it is as simple as:

```bash
sudo apt install flatpak gnome-software-plugin-flatpak
```

After adding flathub as a remote and rebooting, that unlocked quite a number of apps that I think
change the experience significantly. For a quick overview, I ended up installing:

* [Shortwave](//gitlab.gnome.org/World/Shortwave) (internet radio)
* [GNOME Podcasts](//wiki.gnome.org/Apps/Podcasts)
* [Tootle](//github.com/bleakgrey/tootle)
* [Fluffychat](//fluffychat.im/)
* [Drawing](//maoschanz.github.io/drawing/)
* [GNOME Maps](//wiki.gnome.org/Apps/Maps)
* [Bitwarden](//bitwarden.com/)

It seems that many of the apps on flathub are starting to get mobile or reactive-style application
support. The clear winner here is [Fluffychat](//fluffychat.im/), because hot-damn this app feels
polished. It's a full Matrix client with end-to-end encryption support, and can work alongside
[Element](//element.io) if you use that for cross-signing / messaging on desktop or other platforms.
It may seem as if a Matrix client isn't a game-changer, but for me this is honestly one of the main
selling points of a phone like this: being able to use completely secure and open-source
communication platforms (that are federated, to boot!).[^fluffychat]

Needless to say, having more apps at my disposal meant a lot. While the default GNOME Software /
PureOS Software stores do seem to lack a lot of apps, the above list was quite complete for me. I
still do wish Signal was available on either of these phones, but I'm not holding tons of hope out
that it'll happen in any official capacity.

This perhaps might be the biggest reason that I found that the Librem 5 surprised me more than the
Pinephone. While Ubuntu Touch on the Pinephone still has "more apps," the short list above covers a
vast majority of my needs (along with a web browser).

### Anbox

Speaking of apps that exist in Android but not on any Linux distros, Rudi Timmermans had a
[GoFundMe](//www.gofundme.com/f/anbox-on-ut) opened up for bringing Anbox to Ubuntu Touch. It seems
like it's closed now, but I'm fairly hopeful the work here will translate to being able to get apps
like Signal on other operating systems in the future. Of course, Anbox isn't a perfect solution, and
lacks Google's ~~spyware~~ security software called Secure Boot. Many apps for regular users (like
banking apps), or critically important apps for power users (like Snapchat) use secure boot to
ensure your phone is authentic Android (and hasn't been rooted). I am willing to be corrected on
this, but I don't think that Anbox includes Secure Boot in any way, so you are limited in what is
possible with it.

That said, while this is still in its infancy, this is a great example of how all these FOSS
ecosystems are making mobile Linux a third option to Android and iOS.

# What still needs improvement?

## Battery / Thermals

So here's where I start being a little more negative on the phones overall. First, the Librem 5.
Its battery life is pretty abysmal, and it can get _incredibly_ hot. I suspect these end up being
related issues in some capacity, but first the battery. I unplugged my Librem 5 at ~9:30AM in the
morning after charging to 100% battery, and opened zero apps (so doing nothing). The kill-switch for
the modem, was turned off (so modem was disconnected), as was the mic and camera. WiFi and Bluetooth
remained on, although I turned Bluetooth off in the software settings. By ~12:30PM or so, my Librem,
which had been doing nothing and has no services running in the background, was at ~60% battery.
That's three hours of **screen off** time (and no, I didn't have the lockscreen with the clock on.
The display was blank). It's pretty bad. I've repeated this a few times and its repeatable within a
couple percentage points.

Even if I turn the phone on and enable an alarm, and keep it next to my bedside for the entire
night, I cannot rely on the alarm actually firing, because the battery will run out and the device
will die before morning comes. This phone needs to be plugged in frequently. Fortunately, updates
have made it such that charging the phone is significantly easier and faster than it was back in
December. I don't think it's perfect yet (some AC adapters like my Pixel 3 AC adapter still don't
seem to work that great), but the charging ports on my desk work great. Cable support has also
improved, and more cables than just the one that came with the phone can be used now.[^usb-c]

Thermals are also a major concern. During my long road trip with the Librem 5, it performed quite
well at playing music while I was driving. However, even with just a little sunlight in my car (and
the AC going), it still got so hot I thought I had burned my hand touching the metal sides. Given
that I have an Evergreen unit, I think this is something that isn't going to be perfectly solved
until a new Librem 5 with a different design exists. Most of the time I work remotely from my home,
so it's not really the end of the world for me, but the bad thermals plus a weak battery story means
that you can't rely on this thing lasting all day, and you'll need to be careful with where you
leave it (certainly not in the sun!!!).

In contrast, the battery life on the Pinephone seems much better across the board, despite being
smaller in terms of overall capacity. Ubuntu Touch has the best battery life I've seen out of the
distros. In fact, my Pinephone on Ubuntu Touch can easily last a day and a half (~35+ hours) if it's
doing nothing and the screen is off. That goes down with usage (screen-on time is still only ~4ish
hours depending on brightness and other configurations), but overall I've been more impressed with
my Pinephone's battery than almost any other Android device I've had in a long time. Other distros
seem to be pretty close in terms of battery life, but Phosh seems to be a little worse in most of my
tests. It's not very scientific, for sure, but I'd be skeptical if someone said Phosh outlives
Lomiri in terms of battery life by some wide margin.

Thermals are much better on the Pinephone too, but I don't think they've actually improved much
since the last time I wrote about it. Overall, I'd love to see the phone run cooler, but I also
think that the design of the Pinephone weighs into this quite a bit.

## Web Browsing

To start, the best browser out of all the options I've tried remains Morph Browser on Ubuntu Touch.
It's smooth and responsive even on the Pinephone, and feels pretty snappy. I don't need apps for
Twitter or Mastodon on Ubuntu touch for this reason -- Morph is just better.

In contrast, GNOME Web feels like it's gotten worse since I used it in December. I'm not 100%
convinced this isn't because I'm getting more frustrated with it over time or not, but it definitely
can take over 3 minutes to load [Hacker WebApp](//hackerweb.app), which I consider to be fairly
lightweight. I know that it's the browser that's the issue, because while most apps I use on my
Librem 5 (GTK apps, to be specific) are smooth, GNOME Web just consistently sucks. I'm not sure
what's going on here, as it was definitely better than the mobile-skinned Firefox when I first got
my Librem, but now it seems to be the opposite.

Speaking of mobile-skinned Firefox: it's pretty decent now. Startup times for the app are still
abysmal (15-20 seconds on Mobian on the Pinephone installed on an A1 microSD card). However, once
it's been loaded it doesn't have any of the problems GNOME Web does. It's definitely not as snappy
or responsive as Morph Browser on Ubuntu Touch. However, you can install [uBlock
Origin](//ublockorigin.com/) so it's really a bit of a trade-off in what you might be hoping to do.

## Encryption

Well, the Librem 5 still doesn't have full-disk encryption. Apparently Byzantium, the next major
PureOS release, will bring this with it, but the release date doesn't seem locked in yet. Moreover,
I have yet to be able to tell by trawling through the Purism forums if enabling encryption is going
to require a full re-install. I suspect it will, which means more work for users, but that's what
you get when you play with Linux as a hobby.

I'm also somewhat excited to see how Purism utilizes the smart card reader in the Librem 5 along
with full-disk encryption. This is one of those things that I'm not entirely sure is worth it for
the cost (I mean, for most folks just having a separate lock screen / encryption password is more
than enough), but from a technological perspective is pretty cool and unique. Nonetheless, they put
a smart card reader in there, and that definitely helped inflate the phone price, so I'm hoping it
wasn't all for naught.

As for encryption on the Pinephone, the only OS that currently makes it easy to enable is
[PostmarketOS](//postmarketos.org/). PostmarketOS is based off of Alpine Linux, and among the
Phosh-based distros is definitely my favourite. The installer image allows you to set a separate
lock-screen PIN, disk-encryption key, and SSH login. This is definitely the most polished experience
on any mobile Linux today for getting disk encryption. And since PostmarketOS also supports flatpak
/ flathub and traditional means of installing software (through `apk`, not `pacman` or `apt`) the
end experience from a phone user isn't much different than e.g. PureOS.

I am currently thinking that I'm going to abandon Ubuntu Touch for a while on my Pinephone in favour
of keeping PostmarketOS installed on my eMMC because full-disk encryption is so important for mobile
devices. Plus, while I dislike Phosh, the app experience of having Fluffychat available means a lot
more to me while the UBPorts team works towards updating and polishing Ubuntu Touch. That's not to
say that I'll abandon other distros entirely, but given the current state mobile Linux, PostmarketOS
definitely is the distro to watch out for right now (even if Manjaro is the official release partner
for Pine64).

## Installation

This mostly applies to the Pinephone, since I have mostly stuck with PureOS on the Librem 5 at this
point in time.[^pure-os] I seem to have had considerable trouble with installing many of the
Pinephone distros to a microSD card in the past month. I had thought it to be a case of a flaky
microSD, but after rotating through a few it seems that mostly I was having trouble with Mobian
nightly builds, the Mobian May-17 build (official stable at time of writing), and Manjaro Plasma
builds.

I don't think it was the microSD card at the end of the day, because PostmarketOS installed with
their installer painlessly. I didn't dig into it too much because by trying other builds or
attempting the same build again at different times, I managed to get these distros working. I was
using one of the following commands for all this, so perhaps this is on me?

```bash
sudo dd if=distro.img of=/dev/sdd status=progress

# OR

pv distro.img | sudo dd of=/dev/sdd
```

This was all done with a USB &rarr; microSD adapter, but I always had consistently good luck with
PostmarketOS compared to others, so I'm not sure.

Lastly, I will note that if you want to install to an eMMC instead of the microSD card,
[Jumpdrive](//github.com/dreemurrs-embedded/Jumpdrive) is excellent software and you should use it.
You do have to flash it to a microSD to use it with the Pinephone, but it makes flashing images so
much easier. Props to the team of folks who put that together, it's a godsend.

# Conclusion

Overall, I think if you look on a day-by-day or week-by-week scale you're not going to see
significant differences in either of these phones. However, having had my Pinephone for about a year
now, and my Librem 5 Evergreen for about half a year, I'm seeing significant strides in a lot of
different areas. I regrettably cannot cover everything that's gone on in the realms of both of these
phones, but there's always a lot to talk about because of how much community force is behind both
projects.

I still don't think either of these phones are really 100% daily-driver ready yet.[^daily-driver] In
fact, while I've been generally impressed with the pace of improvements from all over I think that
there are still some critical areas where the phones fall short. For the Librem 5, the battery life
and thermals are definitely this device's Achilles heel. For the Pinephone, I think the weakness of
some of the hardware is problematic and isn't something that most people would want to put up with.
Audio skipping and bugs related to the lack of power in the hardware are definitely noticeable. That
said, this is all somewhat biased since if this was 2009 both of these phones would seem as if
they're the future. On the other hand it isn't 2009 anymore, and these phones are here today.

Feel free to hit me up on [Twitter](//twitter.com/thatgeoguy) with questions, and let me know if
there's something else I didn't cover here that's of interest.

---

[^december-usage]:
    Keep in mind, I had the device for a very short period of time by then. Evergreen started
    shipping late November, so I had only had a week or two with the device for that first review,
    which was very much a matter of "first impressions over the ~3+ year period since the
    crowd-funding campaign."

[^bandcamp]:
    For fairness, if you purchase from Bandcamp you can definitely just download the songs locally,
    provided you have a microSD card for expanded storage.

[^openstore]:
    I also realize OpenStore doesn't have flatpak at all, so this might be a bit of a dumb
    complaint to place solely on KDE Plasma. However, I think there's something to say about being
    able to install / run software all from a single app, and flatpak support on the Librem 5 was
    one of the best things I got working in the last half-year.

[^fluffychat]:
    Fluffychat does have one very obnoxious bug that sometimes pops up, namely that text input will
    start entering from right-to-left instead of left-to-right. This results in everything you type
    coming out "backwards" since if you typed "abc" it would fill into the text-entry box as "|cba"
    with | being the cursor position. Annoying, and hopefully something that gets wrapped up soon,
    because otherwise the app feels as polished as many apps on iOS or Android do.

[^usb-c]:
    Although, as with any USB-C product, USB-C cables are spotty as all hell :(

[^pure-os]:
    I had grand plans to install KDE Plasma over PureOS and see if it ran better on my Librem 5, but
    I had such a bad experience with it on the Pinephone that I'm probably just going to wait until
    it's more polished and I am ready to wipe my Librem 5 to install PureOS Byzantium with full-disk
    encryption instead. At that point, there will be far less regret in having to do install / wipe
    / install cycles.

[^daily-driver]:
    It seems that the (semi-?)reasonably priced Librem 5 will be affected by the global silicon
    shortage and will continue to have a long (6-month) lead time as a result. So you can't really
    daily-drive this anyways, because you probably can't purchase one. And no, I'm not suggesting
    the version that costs more than most rent payments. :-)

---

layout: blog
title: Librem 5 Evergreen vs. Pinephone (Part 1 of ???)
author: ThatGeoGuy
description:
tags:
- FOSS
- Purism
- Librem 5
- Pine64
- Pinephone
- smartphones
- technology
excerpt: Huzzah! I recently received my Librem 5 (Evergreen) from [Purism](https://puri.sm). The Librem 5 is a smartphone that runs an otherwise standard linux kernel. However, unlike Android which also relies on the linux kernel under the hood, the Librem 5 uses a GNU userspace, adapted for mobile.  This makes it more akin to your typical laptop in some ways, although the form factor still resembles a modern smartphone (at least, mostly). Here are some preliminary thoughts about the phone and how it compares to [Pine64](https://pine64.org)'s Pinephone, which is another phone that uses neither Android nor iOS, and relies on a GNU / Linux based OS.

---
# {{ page.title }}
<p class='post-meta'>On <em>{{ page.date | date: "%Y-%m-%d" }}</em> by <em>{{ page.author }}</em></p>

Huzzah! I recently received my Librem 5 (Evergreen) from
[Purism](https://puri.sm). The Librem 5 is a smartphone that runs an otherwise
standard linux kernel. However, unlike Android which also relies on the linux
kernel under the hood, the Librem 5 uses a GNU userspace, adapted for mobile.
This makes it more akin to your typical laptop in some ways, although the form
factor still resembles a modern smartphone (at least, mostly). Here are some
preliminary thoughts about the phone and how it compares to
[Pine64](https://pine64.org)'s Pinephone, which is another phone that uses
neither Android nor iOS, and relies on a GNU / Linux based OS.

This is a bit of a long post, and I'm sorry for that. Brevity is certainly a
hard-earned skill, but going forward without comparing the Librem 5 to the
Pinephone seemed like the wrong choice ;)

# What are these phones?

## Purism Librem 5

Purism touts the Librem 5 as a "Security and Privacy Focused Phone." This is
not wholly untrue, and there are some features that are unique to the Librem 5
in this respect. For example, there are "kill-switches" on one side of the
phone that allow you to disable:

- Mobile network and mobile data
- Wifi & Bluetooth
- Camera & Microphone

There's an additional feature where if you disable all three switches, then
other sensors on the phone such as GPS are all disabled as well. You'd be
better served reading through the [Librem 5 product
page](https://puri.sm/products/librem-5) to learn more.

## Pine64 Pinephone

In contrast to the Librem 5, which touts itself as being privacy and security
focused, the [Pinephone is touted as](https://www.pine64.org/pinephone/) "An
Open Source Smart Phone Supported by All Major Linux Phone Projects." It is
considerably different from the Librem 5 in many ways, from the hardware all
the way to the final software. It too has kill-switches, but these are not easy
to access on the side of the phone and instead are inside the case of the
phone. You can access these kill-switches by removing the back plate.

One of the neat things about the Pinephone is the ability to boot and use a
variety of Linux distributions on the hardware itself. It's as easy as loading
the distro onto an SD card, and then booting the phone. You don't even have to
flash anything to the internal eMMC in order to run it!

## Price $$$

### Librem 5

The Librem 5 starts at $799.00 USD, or can be purchased at $1999.00 USD if you
want your Librem 5 assembled in the good ol' US of A before they ship it to
you. There's a lot of talk about anti-interdiction or controlling the supply
chain, but trust me in saying that you're definitely not in the market for a
$2k phone like the Librem 5. I don't know anyone who is.

I could probably write an entire post on the economics of this, but quite
frankly, it's kind of appalling that literally anyone would pay this much. It's
kind of appalling that Purism would charge this much. I can imagine a scheme
where the parts are shipped from China, and verified and assembled by a
part-time college student in the States and they'd likely still be able to
price it cheaper than $1999.00.

Anyways this post isn't about Purism's business practices, of which I will
comment on at some point, but regardless I will drop this matter here. Please
just don't spend $1999.00 on a Librem 5.

And for that one person who will eventually claim they did purchase the
$1999.00 USA version and it was very necessary for their use case: *look buddy,
I get it, you've drank the Kool-Aid on this one but I'm sincerely doubtful that
your very specific concerns and use-case apply to even the general nerd who
gets excited about the intersection of FOSS and mobile hardware. Moreover, I'm
doubtful that even if they did apply to such a niche group that $2k is a price
point upon which you still don't balk at and go look for literally anything
else.*

<hr>
<p class='post-meta'>
<strong>Update (2020-12-08)</strong>:
</p>
<p class='post-meta'>
Many wonderful people on Hacker News as well as the
Purism / Librem Matrix channels corrected me on the fact that part of the
reason that the phone is so expensive is due to the fact that the PCB is
fabricated in the States, in addition to the assembly.  I am still inclined to
believe that the price is rather exorbitant. Perhaps had the USA edition been
something that had been launched after Evergreen was completed I would be less
averse to the idea of it. Nonetheless, I still think that for the general
person reading this: you should not purchase it. You might argue that I don't
know your threat vector, but Bayes' theorem would suggest you will be better
served spending your money on a regular Librem 5 and putting the remaining
funds to something more important to you (like maybe an iPhone, so you can use
Signal while it is unavailable on the Librem 5 :-P).
</p>
<hr>

I bought my Librem 5 back during the original crowdfunding campaign, so I ended
up spending a lot less than even the $799 USD. Surprisingly, I've been waiting
on this phone since around the end of 2017, and here we are in 2020 and I
finally have it. Purism has been sending out what are effectively [DVT and
EVT](https://en.wikipedia.org/wiki/Engineering_validation_test) runs of the
phone that have been going through several phases named Aspen, Birch, Chestnut,
Dogwood, and finally Evergreen. Evergreen is supposed to be the final version
of the hardware, which I opted for. I wanted to get the final, true Librem 5,
not one that could be radically different. While my unit does not have FCC
certification (it is technically what you might consider a PVT unit), it is
representative of the final hardware that Purism will land on for this phone,
as there is ostensibly no more hardware revisions.

### Pinephone

The Pinephone starts at $149.99 USD, sold in small batches surrounding a
particular distro / community that is building software for the device (called
`<distro-name>` Community Edition). Recent batches have made a "convergence
edition" available, for $199.99 USD. The convergence editions have an extra
gigabyte of RAM and a larger eMMC flash storage, and are touted as being useful
for connecting the Pinephone to a full-size monitor and running it as if you
had a full desktop available.

I originally purchased a Pinephone from the UBPorts CE batch. Eventually, I
upgraded my mainboard so that my specs match that of the convergence editions
from later batches. All in all, I've managed to spend more than the original
$200, but I'm not disappointed, since I have had a lot of fun with the
Pinephone.

# Why do I care?

For the vast majority of people, the Librem 5 and Pinephone are probably not
worth even considering. The GNU userspace and associated "mobile" paradigms
within it are very much not ready for daily use.

For the Librem 5, there are some security aspects that this "security and
privacy focused phone" don't even hit (e.g.  hardware backed security / secure
enclave, device encryption, etc.). That's not to say that these can't be made
possible in future updates, but as far as the software story today goes, they
do not yet exist.

<p class='post-meta'>
Small note: Hardware-backed features like the secure enclave do not exist on
the base phone today. However, there is a smart card reader and the M.2 slots
used for the SIM And wifi/bluetooth chipsets on board could be replaced as
well. That said, this isn't exactly in "buy an iPhone and guarantee your
physical security" territory, and there's no clear path to "do X steps to
enable this" yet.

Basically, don't be tricked by the marketing to believe this is somehow the
full package at present. It is not.
</p>

Anyways, so why do I, the author, care? I will admit, after my first bout of
usage, neither phone will be a daily driver for me. I will probably continue to
stick to my Pixel 3, and it is very likely that I will eventually get another
Android device at some point in the next two years. Linux phones are definitely
not yet ready to be daily drivers; however, they are interesting for a few key
reasons.

## Device longevity

Digital waste is a blight on our society. I can admit that the year-over-year
hardware cycle provides a vast set of improvements that can change the
smartphone landscape. However, I also acknowledge that the number of phones,
tablets, and other electronics that find their way into landfills or are
otherwise rendered into trash is a growing concern. Linux phones and mobile
hardware present an opportunity to move away from that, and it's important to
support this in some way.

## Free software

I'm a fan of free software! While my day job isn't working on FOSS, I think
that there's a lot of benefits to having better FOSS software available. The
least of which is because leaps in the mobile space have also attracted others
to work on Linux in the desktop space as well.

## A third option

I care about Linux phones because they present a third option from the Android
/ iOS duopoly. Even if they suck now in (a lot of) ways, they will improve over
time. I want to move towards a future where I don't have to choose between an
OS that's built towards data collection and advertising, and another that's
built on locking you into an ecosystem.

# Okay so... how are they?

First, before I get into my impressions comparing the two, I should at the very
least mention what I am / am not testing. I did not / will not test:

- Phone calls & SMS / MMS functionality
  - I do not have a secondary mobile number, and my primary mobile number is
    registered to an e-SIM on my Pixel 3. So the best I can do is test how
    mobile data works on a data-only SIM.
- Convergence features
  - I don't really plan to use these as anything other than a mobile device, so
    I haven't bothered plugging them into a display. Additionally, my display
    may be a bit too large and high-resolution for either phone to drive it
    effectively.
- Email
  - This one is still on my TODO list, however I've been too lazy to try
    setting this up on several of the Pinephone's possible distros.
- Cameras
  - The Librem 5 does not yet have a functioning camera. For a phone to ship
    with the hardware but no software... Well it's not typical. So I can't
    compare anything, given that the Librem 5 hasn't yet made it possible to do
    so.
- GPS / location
  - Look, I want to do this more than anybody, but with COVID and all I have
    barely needed to leave the house anywhere that navigation would be
    necessary. So I'm very much only comparing surface features on this front.

## General UI / UX

In general, I find Phosh to be awkward to use. I don't like it. It feels
stilted and static. This is most easily seen in how one launches the app drawer
or notification tray. Rather than swiping these (which feels exceedingly
natural), you merely tap them. This feels as if it was done this way because
button-like behaviour is easier than handling accelerations and swiping. This
is probably largely personal preference, but I really, really dislike Phosh as
a base. The Librem 5 doesn't have as many UI alternatives as the Pinephone,
largely in part due to the facts that:

1. The phone only just managed to get to Evergreen (i.e. final production runs)
2. It's expensive and few volunteers can afford one on a whim

So unfortunately for me, I'll have to stick with Phosh on the Librem 5 for the
time being. This is very much a preference problem, but it is a problem.

Lomiri is the name of the UI used by [UBPorts](https://ubports.com/) / Ubuntu
touch (and hopefully one day the Manjaro!!!) distro for the Pinephone. This UI
feels much, much more natural, and I really appreciate the swipe-based
behaviour of everything. Even compared to modern Android and all its quirks and
features I think Lomiri has the right idea for how one might interact with
their device.

Despite my preference for Lomiri over Phosh as a phone interface, I do have to
say that it is very apparent the difference in processing power between the two
devices. This is expected, since the Pinephone is vastly cheaper, but the
Librem 5 is solid with regards to how it handles touch inputs, scrolling speed,
delays, etc. Lomiri does a good job of not feeling slow on the Pinephone;
however, the Librem 5 just feels consistently faster. Better hardware and all,
but the software story doesn't seem like it's starting from a bad place
(granted, this could be very different from anything you read pre-Evergreen,
but forward progress is being made here).

One can certainly hope that one day there will be a Lomiri frontend for the
Librem 5. I am not holding my breath though, since it doesn't seem as if there
are a lot of distros that are advertising that they want to support the Librem
5 (again, probably mostly due to price). I find it a bit of a shame that
they've stuck with Phosh, but hey, Purism went ahead and wrote that themselves,
so why wouldn't they? I do wish something like Ubuntu Touch was available
though, I could see the software story being very different if that was the
case.

## Keyboards

The on-screen keyboard on the Librem 5 is okay and gets the job done, but
leaves something to be desired. Most Pinephone distros also use the same
keyboard, so there isn't a big winner here. However, Ubuntu Touch's keyboard
definitely comes out on top. The fact that it has spelling corrections, word
suggestions, etc. help make it that much better.

Again, this is very much a preference thing, and I hope that the keyboard
receives more updates in the future. I don't see any fundamental reason why it
can't, or why more advanced features won't be made possible.

It may also be worth noting that if you know nothing of these devices: swiping
to type is not a feature of any keyboard on Linux phones. Maybe one day, but
no, not now.

## Kill switches

One thing that has frustrated me about the Pinephone is that the kill switches
seem to be a secondary feature of the phone. What does that mean? Basically:
these kill switches aren't really as nicely integrated in the software as they
could be. I don't actually care that you have to remove the backplate to get to
them, but I've sometimes noticed a tendency for the software to not gracefully
handle the fact that one of them could be switched off. I've said a lot of good
things about Ubuntu Touch until now, but it really doesn't like if you disable
the mobile data / baseband switch. Other distros handle this in various ways,
but especially with the original mainboard my UBPorts CE Pinephone had I
noticed that disabling the baseband could sometimes render wifi from never
re-connecting after the device has gone into a deep sleep (i.e. screen is off
for a few hours).

I'm not in the business of debugging this or ascertaining why, but it seems
that the kill-switches on this phone have not been a day-1 feature. It's not
the end of the world given that I now work from home every day (so like, what's
a phone for anyways?), however I would appreciate if the Pinephone worked as
well as the Librem 5 in this regard.

And on that note, the Librem 5 kill switches *appear* to work fantastically. I
say "appear" because it is basically impossible to test the camera and mic kill
switch (mostly due to the lack of a functioning camera). I have not tested any
communication apps yet, as I don't believe there are any that I currently use.
When there's a fully functional Matrix client on both phones, I'll be happy to
go through and test both :-)

Nonetheless, the baseband and wifi kill switches do exactly what you expect
them to do on the Librem 5. **Further, when turning them off and on again, the
device and software recover fantastically.** Big win here for the Purism folks,
as this can't have been a small feat.

## Mobile data

Not much to say here. A data-only SIM from Google Fi seems to work pretty well
in either phone. I haven't had any major issues with connecting to a network
with either the Pinephone or Librem 5.

## Apps

There is not a vast ecosystem for apps on Linux phones yet. Ubuntu Touch boasts
the largest app store, but quite frankly a lot of those apps are not really in
my wheelhouse. However, there are some aspects of apps from my first
impressions that stand out.

### Location / Navigation

I know I said that I wasn't gonna discuss GPS stuff, since I've barely used it,
but my basic first impressions are:

- The Pinephone (with uNav3) is a pleasant experience, but getting that first
  location lock can take forever. I believe this to be unique to the Pinephone
  hardware, but I'm unsure if I'm just doing something wrong.
- The Librem 5 seems to default to getting mobile network location. It does get
  a position lock quickly, but it seems like it's not using GPS, so appears to
  be inaccurate. This was most apparent when trying to use the Lyft web app,
  and setting my pickup location using "current position." I'm glad that the
  browser seems to support this functionality, but it needs some refinement.

### Web apps

The Librem 5 impresses me a lot with it's web app support. While I initially
had some issues with the browser, an update did manage to correct it (seems to
be recent, so I don't think it's the norm?). Anyways, the GNOME Web browser
does a good job of being able to "pin" web pages to your launcher. This
performs a very lightweight sandboxing as well, limiting that "app" on your
launcher to only a few sites. I really like this approach as it enables quick
access to mobile websites like Lyft, Twitter, Mastodon, etc that I might have
just used the browser for anyways.

This also works if you use GNOME Web on any Phosh-based distro for the
Pinephone, but doing this on the Librem 5 seemed to be much more pleasant, if
only because of the increase in computing power. The general gist is that very
few native apps currently exist for either phone (or distro, really) but using
mobile websites seems to work pretty well.

### Music / Playback

Alas, neither phones have a good Bandcamp or Spotify equivalent yet. UBPorts
has a Spotify client called Futify, which is still in beta and not on the Open
Store (you have to get the click package manually). However, Ubuntu touch also
doesn't handle switching to headphones when a pair of headphones are plugged
into the 3.5mm jack on the phone.

The Librem 5 does appear to handle switching to headphones, but the lack of
streaming apps is a bit of a pain. Likewise, it seems that my FAT-formatted SD
card is not automatically mounted / detected by the Librem 5.

Needless to say, you can listen to music on these phones, and the headphone
jack is a very, very welcome addition, but you'll need to do some work to get
going here.

Also, for both the Pinephone and Librem 5 (Phosh based distros), I highly
suggest using [Lollypop](https://wiki.gnome.org/Apps/Lollypop). It is heavily
focused on sorting / playing via "albums" as opposed to individual songs, but
it's a very pleasant app to use. A lot of thought has clearly been put into
this app.

## Charging the battery

Charging the Librem 5 seems to take forever. No, really. It seems after asking
in the official Matrix room (`#community-librem-5:talk.puri.sm`) this is
something that is being worked on. General conclusion: USB-C is the worst, and
power is hard. I'm inclined to agree on both those points but it is a little
disappointing that charging using anything other than the charger + cable that
came with the phone is a bit of a toss-up right now. Future software updates
should hopefully fix this.

On the Pinephone side I've read that [charging the battery can be
dangerous](https://xnux.eu/log/#017). This is more than a little concerning.
However, it's also pretty clear these devices aren't being marketed as an
Android replacement just yet. I haven't had anything bad happen to my
Pinephone, but it's worth keeping an eye out for.

## Temperature

Both phones can run fairly hot. I think given how I hold the phone (closer to
the base of the phone) I tend to notice the Librem 5 more often than I notice
the Pinephone getting hotter. This is mostly because the frame / chassis for
the Librem 5 is metal, and conducts the heat around the sides of the phone. So
you'll notice it getting hot after you've used your phone for a good bit.

All in all though, if you feel where the CPU is on the Pinephone, it's not
vastly different in terms of how the temperature *feels* while using the phone on
high load. That doesn't mean the temperatures aren't different, but I haven't
measured enough to say either way.

## Phone shape

Aha! You thought I wasn't going to talk about the shape and size of the phone.
Well, here it is:

The Pinephone is shaped like a phone. It is a little wide and tall for my
tastes, especially given the screen resolution is much lower than e.g. my Pixel
3, but it is phone shaped. It fits in my pocket well, and otherwise feels like
a smartphone does in 2020.

The Librem 5, however. Well, it's a chonker, that's for sure. This thing is
massively thick. It's about twice the thickness of my Pixel 3, and it's not all
battery. This is shaped more like an external hard drive than a phone. This
definitely bulges out of any pocket and is somewhat comically large. It's also
heavy, and I find it much more tiresome to hold for longer periods than say, my
Pixel or the Pinephone. From an aesthetic point of view, it just looks like a
giant brick.

I know I'm ragging on the Librem 5 a lot here, however don't expect that the
iPhone-level price tag means you're getting an iPhone-level device. Be warned:
if you are expecting this phone to be "thinner than it seems" you will be
disappointed.

# Community

## Pine64

Pine64 (for all their devices, not just the Pinephone) is really a vibrant and
positive community in my experience. The leadership that Pine64 shows in their
community updates, their trasnparency, and how they collaborate and give back
to the projects powering their phone are very strong reasons for supporting the
project. I always love reading the monthly updates, even if I know I'm not
buying any more Pine devices anytime soon.

I also especially appreciate how they have been donating money from every
Pinephone Community Edition run to the projects behind those community
editions. It's a great way to help sponsor and foster FOSS, and I am so happy
that this is made possible.

I really don't know if there's anything I can say about Pine64 that is
inherently awful. They've done a fantastic job doing what they're doing.

P.S. Their web store used to suck but its gotten a lot better. Making a web
store is insanely hard though, so props to them for being able to manage it as
they have been. Where's the FOSS Shopify when you need it?

## Purism

Purism on the other hand... A lot can be said about their interactions with
their community, or the community at large. In fact, a lot
[has](https://jaylittle.com/post/view/2019/10/the-sad-saga-of-purism-and-the-librem-5-part-1)
[been](https://jaylittle.com/post/view/2019/10/the-sad-saga-of-purism-and-the-librem-5-part-2)
[said](https://jaylittle.com/post/view/2019/10/the-sad-saga-of-purism-and-the-librem-5-part-3).

Extending from this discussion, there's a lot of people who wanted a refund on
the Librem 5 after doling out large sums of money to pay for it. They may want
a refund for a number of reasons, whether due to the economic downturn of
COVID, the delays in getting this phone out, or just general distaste for
Purism and how they have behaved regarding community updates and the lack of
ownership over the mistakes made. However, you can find a couple of posts
on Reddit from users who can't seem to get a refund:

- [Are they still giving refunds for the Librem 5?](https://www.reddit.com/r/Purism/comments/k2z9i4/are_they_still_giving_refunds_for_the_librem_5/)
- [Purism won't refund me](https://www.reddit.com/r/Purism/comments/htqz2q/purism_wont_refund_me/)

Purism claims that they provide refunds when they get to your slot and ask
where to ship the Librem 5. I think this is dodgy as all hell. Refunds need to
be provided in a timely manner, as per a matter of law. For folks like me who
got in during crowd-funding, I knew that money was gone.  Getting a Librem 5
was more or less a crapshoot.

I am, however, very upset with the way that the business has conducted itself.
This really isn't something that should be happening at all.  Rather than
shipping out EVT / DVT units in the form of Aspen, Birch, etc. the company
should have admitted they did not have a clue what they were doing, and
extended their timeline. Nothing they've done since then has made me believe
that this company is behaving any better. Yes, that includes the fact that they
shipped me a phone.

I also notice that there's a lot of folks who are very ripe to jump in and
defend Purism at a moment's notice. Saying things like: "mobile linux wouldn't
exist without them," or "I am doing this for \<some-principle\> despite the
fact I disagree with their lack of transparency." I think this is a bit of a
toxic way to approach it. Look, I have the phone, I left my money with Purism
and still want mobile linux to succeed. But the community at large has to call
this sort of behaviour out if they don't want it to continue happening.

Lastly, and this is very much a distraction from the rest of this article, but
I notice that the Librem 5 as a phone tends to draw a particular crowd. People
who label themselves as "[freedom
thumpers](https://www.reddit.com/r/Purism/comments/ikjcwi/i_feel_like_im_never_getting_a_phone/)",
or folks who seem to believe that "security and privacy" as concepts are above
reproach or scrutiny. It's not everyone, and many people are pleasant to talk
to especially on some of the Matrix channels, but there's an oddly fierce
following of folks who aren't willing to look outside the bubble on this.

I can sympathize with the intent of these folks to some degree. I mean hell, I
bought into the Librem 5 hoping that someone would start working on a third
option that wasn't going to be run by a mega-corporation intent on monetizing
every aspect of your life. If I'm being honest though, I dislike how much the
community jumps on these memes and touts a $1999 USD (unfinished) phone as the
only path towards true security and privacy.

Purism has a lot to do to step up their own transparency, and parts of the
community has a lot of work to do to stop sounding like some crazy uncle at
Thanskgiving; or at the very least, show the more positive aspects rather than
permit [lengthy diatribes around how we should support them despite Purism's
horrible communication, and think strategically about what we really
want](https://www.reddit.com/r/Purism/comments/je6w2u/what_chance_does_mobile_linux_have_without_purism/).

I didn't want to make an article about this all on its own, but I definitely
needed to say something here. In the future, I want to avoid having to have
this same discussion again, so I'm going to try to leave it at that.

# Conclusion

It's obviously not really appropriate to compare the Librem 5 and the Pinephone
spec for spec. They come in at two very different price points and are marketed
as two very different devices. However, comparisons can't help but be drawn
between the two, as these are really the only two "no Android, no iOS" phones
out there. For me, these devices represent the beginning of a world in which we
can avoid Android and have devices that are truly under our control. Moreover,
I hope that the lifespan of both my Pinephone and Librem 5 outlasts my current
Pixel 3 and whatever other Android phone I may buy in the interim.

Overall the experience on both is fairly positive. Both phones are clearly not
daily driver ready, but are a lot farther along than anyone might expect. I
don't much like Phosh, perhaps for superficial reasons, but I have a pretty
positive view towards the future of software on both the Pinephone and Librem 5.
Perhaps once I get a little more reign on my free time, I will try to see
how hard it is to write an app that can be used on either device.

Outside of maybe reddit, both communitites have been fairly eager to
jump in and help when questions are asked, and Pine64 is definitely amazing in
this regard.

## Should I buy a Pinephone?

This will largely depend on whether or not you want to help fund a community
edition version of the Pinephone, and what your goals for such a device are. I
think in general I would lean towards yes, you should, but I wouldn't use it as
a daily driver. Too few apps and so many weird corner cases in everything, this
phone is more for hackers than it is for consumers (and I mean, they say that
on the box). For now, keeping this as a secondary phone in data-only / WiFi
mode seems to be the sweet spot. The software continues to improve over time,
so if you want this device to help test that on a single or even multiple
distros, then definitely get one!

Pine64 sports a great community as do the various distros for the Pinephone.
There's a lot of goodwill in this community to make mobile linux be even more
of a thing.

## Should I buy a Librem 5?

If you're considering spending $1999 USD on one, don't. :-)

Additionally, you have to consider if you're willing to invest in Purism
despite their lack of transparency and past mistakes. I don't think that Purism
is immutable in this regard, they certainly do have a very vibrant community on
Matrix that doesn't suck, but it's very hard to get anything official from the
company with the level of transparency and accountability that e.g. Pine64
provides. When Purism makes mistakes, they don't seem to like to admit to them.
I am not so cynical as to believe that they are all bad actors, merely that the
organization has a problem with taking credit for their failures.

From a purely technical perspective, the Librem 5 is pleasant to use, and is
surprisingly very well integrated (despite how massive the actual device is).
The kill switches are an excellent feature and they work incredibly well on
PureOS. Additionally, I can see many of the technical problems I've listed
above being fixed in future updates, and will continue to stay up to date on
those as they come.

Overall, I do not regret buying into the Librem 5 campaign. The phone itself is
a marvel considering how long it took to come to market (at least, compared to
my expectations). I have been disappointed many times along the way, and I do
find some of the community behaviour a bit toxic and / or predatory, especially
when it comes to refunds or putting up with poor behaviour on Purism's part.
Despite this, I have been having fun with both the Librem 5 and Pinephone, and
am looking forward to a bright FOSS future for my choices in mobile OS.

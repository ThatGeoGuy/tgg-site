---

layout: blog
title:  Geomatics – Measuring, Modeling, and Mapping
author: ThatGeoGuy
description:
tags:
- geomatics
- measuring
- modeling
- mapping
- maps
- surveying
- photogrammetry
- remote sensing
- GIS
- defining words
excerpt: I wanted to write an article describing what Geomatics is. Or at least, try to touch on the core ideas of what being a Geomatics engineer is all about, or what you might end up studying if you decide to get into Geomatics yourself.

---
# {{ page.title }}
<p class='post-meta'>On <em>{{ page.date | date: "%Y-%m-%d" }}</em> by <em>{{ page.author }}</em></p>

I wanted to write an article describing what Geomatics is. Or at least, try to
touch on the core ideas of what being a Geomatics engineer is all about, or what
you might end up studying if you decide to get into Geomatics yourself.

The running joke of most of my career has been that nobody knows what a
Geomatics is, what it does, or I find myself having to explain that *no, I don't
really work with rocks all day.* During my time at the University Of Calgary, it
was a pretty common in-joke amongst the students, and industry consistently
holds a panel year over year to try and answer this same question again and
again and again.

So what answer do I keep coming back to? Well, it's probably worth first
thinking about some of the ways I think people break it down, and why I feel
these answers are insufficient.

## Geomatics as a set of disciplines

The easiest way to perhaps think about a given profession is to see what people
who are in the profession *actually do*. If you break this down for geomatics,
you'll probably get the following list, or at least one or two items from it:

- 3D Imaging Metrology (e.g. Photogrammetry, LiDAR, etc.)
- Digital Imaging (e.g. Computer Vision Techniques, Deep Learning)
- Geospatial Information Systems (GIS)
- Location Based Services
- Remote Sensing (e.g. Satellite imagery, medical imaging, radar, etc.)
- Internet-of-Things (IoT)
- Conventional Land Surveying / Land-use planning

However, I think this definition falls short. This effectively makes geomatics a
blanket term for any industry or professional in one of these industries. It
doesn't inform the conversation so much as mask it.

I remember going to one of the geomatics industry panels the University put on
during my time as an undergrad. The purpose of the panel was to try and help
people understand what Geomatics is. The classic problem is that people look at
Geomatics and just assumed that it was land surveying, and that was it. But the
industry is so vast and varied, that this definition doesn't necessarily apply
anymore, and many of the same principles that go into surveying apply to other
specializations within Geomatics.

At the end of the panel, the panel members had made their case for what
geomatics meant to them, from each part of their respective industries. They
then posed the question towards the students attending, prompting a response
that was a bit too on-the-nose from one of my classmates. The discussion went:

> Panel: So, what *is* Geomatics?

> Classmate: Surveying!

Naturally people laughed, however this outlines probably my biggest disagreement
with defining Geomatics based on the specializations or sub-components of the
industries or people working in it. No one of these industries can claim that
geomatics is all about them, and yet for some people, that's what geomatics
means.

I think my biggest objection here is an attitude among people in geomatics that
it can be one thing, or another thing or one of a selection of things. In most
cases, that ends up being tautological and/or doesn't help drive any discussion
to help people understand what the whole point of the matter is anyways.

<p class='post-meta'>
This digs into another problem that surrounds Geomatics as a discipline.
Geomatics started out thanks in part to land-surveying, and has grown alongside
it since. There can sometimes be somewhat of an attitude on both sides of this
that decide that you're not a <strong>real</strong> geomatics engineer if you
don't land survey (or don't survey with traditional tools like a total station).
Sometimes people say it in jest, but this kind of gate-keeping is really
detrimental to helping the field flourish.
</p>

## Geomatics as a bad Wikipedia definition

At the time of writing, the [Wikipedia article on
geomatics](https://en.wikipedia.org/wiki/Geomatics) defines it as:

> Geomatics is defined in the ISO/TC 211 series of standards as the "discipline
> concerned with the collection, distribution, storage, analysis, processing,
> presentation of geographic data or geographic information".[1] Under another
> definition, it "consists of products, services and tools involved in the
> collection, integration and management of geographic data".[2] It includes
> geomatics engineering (and surveying engineering) and is related to geospatial
> science (also geospatial engineering and geospatial technology).

Geomatics is derived from a portmanteau of *Geospatial Informatics*. So under
that understanding, the above definition isn't inherently wrong, and *does*
manage to encompass our previous definition based on industries and what we
might know about them. Almost.

The problem here is that this definition is strongly centered on *geographic*
data or information. But geomatics isn't always concerned with geographic
information. Close-range photogrammetry would fit this bill, and be considered
geomatics engineering if you were reconstructing the geometry of say, a bridge
or a historical glacial erratic. However, if you applied exactly the same
techniques to medical motion capture, or to scanning small objects to place in a
VR scene, is it no longer geomatics? The same argument could be made about
remote sensing; why is multi-spectral satellite imaging considered geomatics
whereas an MRI machine or CAT scan is not?

As per usual, Wikipedia might be a good starting place, but your highschool
English teacher was probably correct in telling you that it's not good to write
your entire essay with Wikipedia as your only reference.

## Geomatics as measuring, modeling, and mapping the spatial world

I always end up coming back to describe geomatics as measuring, modeling, and
mapping [the spatial world]. The spatial part I tend to leave mostly implied in
casual conversation, but it does make the definition more complete.

The only other thing I'll say here is that I want to emphasize *spatial*, as
opposed to *geospatial* or *geographic*. I don't think it matters if you're
scanning limbs or surveying a construction site or doing satellite imagery over
a natural disaster, it's all spatial information and it's all geomatics.

### Measuring

The first cornerstone component of geomatics is our ability to measure.
Geomatics is rooted in science, and is predominantly concerned with, as I
mentioned above, *the spatial world*. What does that mean?

Geomatics really came out as a branch of civil engineering, in particular when
land surveying branched out and became what some would call "survey
engineering." Surveying is a lot of things, but the primary purpose of surveying
was to be able to define the boundaries of the world around us and help increase
our understanding of the geometry and characteristics of the spaces and world we
live in.

The key to doing this with any sense of scientific rigor is to take lots of
observations, or measurements. A geomatics engineer should know how to make a
good measurement. This can derive from a number of different factors:

- What instrument are you using to measure? Do you understand how that
  instrument produces measurements?
- What are you actually measuring? Does what you're measuring reflect what
  you're trying to solve for?
- Statistically speaking, are your measurements independent?
- What is the intrinsic error associated with a measurement, and how does that
  affect how many measurements you need, or to what confidence you can solve for
  something?
- Is the network of your measurements sufficient to solve the problem you are
  trying to solve?

These are some examples of questions that every geomatics engineer will have to
ask themselves when designing a project, or figuring out what is necessary
before you can begin to plan a project.

The ability to think about a problem in terms of "*what do I need to measure?*"
or "*what are the limitations on my measurements?*" is one of the only things I
can say is repeated across every industry that touts itself as being geomatics.

It is also important not to forget the spatial aspect of these measurements
either. You might be inclined to think, "well that's just science so this isn't
helpful," however, I always think of this in the context of spatial components.
Whether it's knowing when to update a mesh with new information from a range
camera, or thinking about loop closure in a triangulateration survey, being able
to make reliable measurements of spatial properties is one of the most
fundamental aspects to good geomatics engineering.

### Modeling

The next aspect of geomatics is modeling. Having measurements is great, but it
is also important to be able to use those measurements to produce information
that will be useful.

In 3D image metrology, particularly photogrammetry, a good example of this is
the bundle adjustment, which models the relationship between camera position,
your lens model, and the geometry between your image plane and the scene that
you're capturing. Another example might be GPS position derivation, or
navigation using a Kalmann filter. These models take our raw measurement data
and produce something (e.g. pose, velocity, motion) that we care about.

These spatial models usually exist to answer one or more questions about the
world, that our spatial data or measurements can help inform. Understanding the
models, why they work, what statistics they're based on, or even just how to use
them is a fundamental aspect of geomatics.

### Mapping

The final output from many geomatics projects is some kind of map. Whether this
is an online map, a site survey, or even a 3D mesh or point cloud, mapping is
often the final output that a geomatics engineer will give to a client. In
short, by taking the information that our model provides us based on our
measurements, a map presents this data to the user in a spatially meaningful
way.

Some aspects of geomatics, such as GIS, or navigation, concern themselves almost
entirely with maps, mapping layers, etc. In these cases, existing maps *become
the measurement*, and value is derived by producing models or new maps through
the use of mapping layers, or by adding semantic information to existing data.

Sometimes, mapping isn't even about the map, but how the map changes over time.
I think my definition of mapping might be somewhat fast and loose here, but I
haven't found too many instances in geomatics where somebody doesn't have a
"map" to present at some level. If nothing else, the visual clarity of a good
map tells you a much richer story than many words can.

## Final words

This question has been pretty close to me for a long time. When I first started
university I had no idea what the heck geomatics was, or why I might actually
care. Eventually though, I ended up choosing geomatics as my major and have not
regretted it since.

Yet, the problem remains that at any given time, nobody knows this field, why it
exists, or what people in geomatics do. Half the population thinks I am geology,
half the population thinks I survey in the wild. The truth lies somewhere on a
third axis of that Venn diagram.

Geomatics is an extremely rich industry and I think people need to pay more
attention to it, because spatial data is eating the world. Which is why when
explaining myself, I always try to inform people that Geomatics is Measuring,
Mapping, and Modeling [of the spatial world].

But, on the other hand, the next time my Lyft driver asks me what I do for a
living, I'm still liable to just end it with "I'm in tech" and go on to write
code another day.

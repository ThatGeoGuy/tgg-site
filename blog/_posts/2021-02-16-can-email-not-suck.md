---

layout: blog
title: Can email not suck?
author: ThatGeoGuy
description: Spurious thoughts I had thinking over email and email clients.
tags:
- email
- organization
- communication
- hey
excerpt: |-
    The (current) extended public health crisis has given me a gratuitous amount of free time. At least, comparatively
    to before the public health crisis. This past weekend, bored out of my mind, I came across a [post from the K-9
    developers](https://k9mail.app/2021/02/14/K-9-Mail-is-looking-for-funding) on Hacker News. This got me thinking a
    lot more about email and how my use has changed over time.

---

# {{ page.title }}
<p class='post-meta'>On <em>{{ page.date | date: "%Y-%m-%d" }}</em> by <em>{{ page.author }}</em></p>

The (current) extended public health crisis has given me a gratuitous amount of free time. At least, comparatively to
before the public health crisis. This past weekend, bored out of my mind, I came across a [post from the K-9
developers](https://k9mail.app/2021/02/14/K-9-Mail-is-looking-for-funding) on Hacker News. This got me thinking a lot
more about email and how my use has changed over time.

I use K-9 on Android, and if you do too, you should definitely help fund the app. It's one of the few choices on Android
for email that doesn't suck.  However, this isn't really about K-9 or their funding. This article kind of got me
thinking about email and how I use it, and with an idle mind that had no other immediate priorities I started
re-evaluating how I use & interact with email.

I'm writing this mostly because my blog is a blank canvas for my thoughts, and because I can.

# Who cares about email anyways?

In recent years I've been taking this attitude more and more. Email certainly is the universal platform (everyone has at
least one email address), but I don't send an awful amount of email myself. Back when I was in grad school email was
literally the only way you could talk to some people. I met professors, other students, and faculty from all branches of
the university that would put almost their entire lives into email. Nowadays, I suppose this is more likely to happen
over Slack, or even [Element](https://element.io)?

I send a lot less email than I used to. I also receive a lot fewer emails than I used to, or at least, it feels like I
do[^1]. All my work communication is handled through Slack, and most of my personal communication will be over Signal
or Element.  Despite this, email is still pretty good at a number of things, and its not like I could see myself getting
rid of it. Ignoring "you need an email address to sign up for X service," email is still king at:

* Availability & compatibility. Everyone has an email, everyone can probably receive an email, and everyone can
  communicate via email. Okay, that last one is a bit of a stretch, but I'll leave a caveat here that learning to
  communicate effectively via email is significantly easier than e.g. Twitter.  So it's got that going for it at least.
* Asynchronous communication. _Sure, instant messaging is asynchronous_, but is it really? I feel completely comfortable
  not replying to an email anywhere from an hour up to and including never. I can't say the same about small Slack
  notifications.

There's good reason to think email isn't going away anytime soon, so I'm fairly confident that lots of people do care
about email, and will continue to do so.

## Hey (product) cares about email

Thinking about email and how I still use it, I was reminded that [Hey](https://hey.com) exists. Hey is a subscription
email service (think, paying for Gmail) that aims to do email radically differently than everyone else. There is a
limited amount you can change about email from the get-go, but I went back and watched an entire [37-minute
video](https://www.youtube.com/watch?v=UCeYTysLyGI) of their CEO explaining how Hey is different than regular email.

The video is long, and touches on a lot of features. I don't personally see myself ponying up ~$99 USD a year for email,
but I think there's both some good and bad ideas in there. Some of that probably depends on the volume of email you
receive, and like I said, I don't receive lots. Nonetheless, Hey looks to me more like they changed the frontend for a
web-email client more than they changed anything about email itself. This is completely fine, and is not really to
detract from their product (a lot of the choices are good!), but don't go in expecting the behaviour of the protocol to
vastly change.

Hey gets a lot of things right. I really like the idea of "surfacing" attachments and details from emails (i.e.
displaying them as a separate category and making them much more searchable). If I ever go neck deep into using email to
manage my life, this is something that would be a game changer.

The idea behind "the feed" is also really good. I've signed up for a few substack lists as well as some great columns
like Matt Levine's [Money Stuff](https://www.bloomberg.com/account/newsletters/money-stuff). The video demo that Hey
shows is a bit wrong, as I don't expect that most people would want to use the feed for marketing and advertisements,
but maybe the way I use email is weird. Nonetheless, the concept of having a folder / dedicated filter for feed items
seems both plausible and pragmatic to me. This also made me feel a little dumb because I had always separated all feed
items / mailing lists into separate folders. For open-source mailing lists I still do this (don't cross your
[chickens](https://lists.nongnu.org/archive/html/chicken-users/) with your [guile
users](https://lists.gnu.org/archive/html/guile-user/), you don't want anyone getting salmonella!); however most of the
"feed" columns I subscribe to don't post nearly often enough to make my feeds folder explode.  And to the point Hey
makes, you shouldn't have to stress out about this folder or care.

Similar logic applies to their concept of a "paper trail," but I'm also convinced that's something that has to be mostly
curated manually since automatic filters for sorting receipts & invoices are... ripe for error on that one.

Lastly, the idea of being able to permanently ignore threads is 💯 (the best).  There are so many instances of people
not knowing the difference between "Reply" and "Reply All" that I can veritably say that the lack of this feature in
every other email client has driven someone mad and / or driven someone to death.  Hard to quantify, but the probability
seems high enough at scale.  Anyone on the Thunderbird team willing to integrate this without having to make
thread-specific filters? Anyone on the Dovecot team willing to make this a plugin?

# So _does_ email always suck?

Probably? Email obviously isn't a secure communication medium, and the protocols are pretty old school.
[JMAP](https://en.wikipedia.org/wiki/JSON_Meta_Application_Protocol) seems like it has it's heart set in the right
place, but I am skeptical of most of the internet moving in that direction for... oh the next 25 years maybe? Years may
be a bad prediction metric, so maybe we say instead that Google will have launched and killed at least 10 more chat
applications by that time?

I think something more interesting surrounds the fact that most people suck at email. I cringe whenever I see an unread
count in the tens of thousands. Some people believe that to be a badge of honour, but I'm fairly certain that just
describes you as someone who doesn't care, and I generally take the position that it's not a virtue to be proud of how
much I don't care about things.

For a lot of people who are hopeless with email, an offering like Hey might be good. On the other hand, I think if
people grew up learning even the base level of email skills (etiquette?) then a significant part of the world would be a
lot better off.

Knowing how to use email effectively is probably a superpower for some folks in some organizations.

# Easy ways to get better at emails

These are just some of my tips, but they certainly helped me get more proficient at email over time.

## Use an actual email client (not a web client)

Upfront: Maybe this isn't the most universal advice. I got a lot farther with email when I started leaning heavily into
using Thunderbird rather than trying to keep up with every change to the Gmail interface. What makes a good client is
somewhat subjective, but I'd say that working with web clients has overall been a poor experience for me.

For every feature a web client has, it also has a million ways to be distracting or use 90% of your CPU in a single tab
of your browser. "Hey" might be different in some regards here, as their UI seems quite intuitive at a glance, but
you'll pry my desktop native (read: not Electron) client from my cold, dead hands.

## Don't send HTML emails

This is just good writing advice but: focus on content over form. The number of random business emails I get with fancy
HTML signatures, tons of markup, etc.  are ridiculous. Stop wasting yours and everyone else's time, please, and just
write the email. If your email requires more formatting to be readable, then you've already failed on the content front.

Being polite is a skill unto itself, but your email is (usually) not a thinkpiece. Just write the text and focus on the
message. The people writing fancy substack newsletters and such obviously don't need to follow this advice, but most
people seem to think that plain text emails are somehow... bad?

## Use message filters to organize things

This goes back to the idea of "feeds" or a "paper trail." Message filters and folders are a great way to get unimportant
stuff out of your inbox. It's not even something that should take you a full afternoon to learn.

The biggest downfall of this is that message filters can't be easily translated from one client to another. Likewise,
server-side options with LMTP / Sieve or whatever are still complex as all hell to set up, and then you _still_ need to
understand how to write the scripts to do the email filtering. This is more a complaint for someone who has their own
mail server and uses multiple clients (K-9 on Android, Thunderbird on Linux, Geary on Phosh-based distros).

Most people who want to set up filters in Gmail should absolutely just go do that via the web interface, since that will
happen before mail reaches your local client. The filters you choose to set up or how you organize also doesn't matter.
I think the feeds and paper trail ideas have some merit, but the main point is **stop pretending your inbox is the only
folder that can ever exist**.

And on that note: please for the love of god filter and silence automated emails (think: a CI pipeline), or at least
funnel them to a different folder.  Your inbox is not a notification bar, and on pretty much every mobile client ever
you can have alerts get sent to you either 1) not through email or 2) with a filter that weeds out noise. So many people
hate email for this reason alone!

## Stop using your email as a TODO list

I mean, this speaks for itself. Use Org mode, use one of literally hundreds of TODO apps, use a paper and pencil! But
don't use email[^2].

## Don't be afraid of archiving or marking email as read

People always tell me [inbox zero](https://web.archive.org/web/20140317005022/http://inboxzero.com/articles) is hard.
It's not hard, just right click your inbox, mark all as read, and archive all[^3].

🔥 🔥 🔥 🔥 🔥 🔥 🔥 🔥 🔥
{: align='center'}

Exchanging my inbox as a-place-where-email-goes-to-die with my archive isn't immediately the best choice, but my inbox
is a working directory and I have filters for things that shouldn't be lumped into the archive, so the difference comes
out to be a positive impact at the end of the day.

## Try to avoid using mobile clients?

This is maybe something I've picked up on since I work from home now, but most email isn't urgent. If you need to
urgently contact me in a personal manner...  there are ways and that is kind of why the telephone network exists. If you
need to urgently contact me professionally, my work email is open when I work but you're still better off bugging me
elsewhere.

While I do adore K-9 for being the best mobile client for Android out there, I actually think mobile clients for email
pretty much universally suck (sorry if your business is mobile email clients, but I'm probably not going to be your
customer). Email is best experienced with a physical keyboard, and most of the time I won't respond to your email if I
don't have a keyboard. I used to do a lot of email via mobile, but I've since moved away from doing so because I'll
almost always have either my Pinebook Pro with me or I'll be at home with my desktop.

# Tell me about how email ruined your <life / marriage / etc>!

I'd be interested to see if there's others out there using email in a more professional or at least more sane way than
most people do. I'm also interested to come back to this article in another five years and see if my current choices
have held up or if the way I work with email now is just a function of the volume of email I don't receive.

Give me a [shout]({{ site.baseurl }}/#contact) via any of the usual modes listed, and let me know what you think.

---

[^1]:
    I should probably measure that, seeing the stats would be cool! At least, they'd probably more useful than my end of
    year Spotify stats, and I care enough about those to get a little interested at the end of the year, so it wouldn't
    be <em>entirely wasted effort</em>. If you're someone looking for a project, make this! This is absolutely something
    that would help me figure out how to optimize (read: stop reading) my emails.

[^2]:
    Cue the guy who has an org-mode TODO -> email integration set up in Emacs to tell me how he automated his TODO.org
    to get sent to his email every morning so emacs can parse that email and automatically do every task for him. All
    written in emacs-lisp and some C, naturally.  😂


[^3]:
     Obviously, when I say this to most people they act like this is the up-front "nuclear" option. Look, I don't need
     to tell you how to live your life. However, if you're going to let email pile up in your inbox to the point you
     can't find anything and then <em>you'll feel bad about the fact you can't find or reply to anything</em>, then
     those emails are no worse off in the Archive, and at least the next emails that come in can be dealt with properly
     and you won't have to feel bad about those.


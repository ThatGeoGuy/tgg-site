# tgg-site

Defines the layout of my blog using jekyll - https://thatgeoguy.ca.

## License

Licensed under GPLv3. See `LICENSE` for details. All content for the site (`index.html` and anything
in `blog/_posts/`) is provided under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0
International](http://creativecommons.org/licenses/by-nc-sa/4.0/). This basically means you can use
my content if you:

1. Attribute the original work back to me.
2. Don't use the work for commercial purposes (e.g. to make money or as a final implementation in
   production if the post is about code)
3. Share the final content that you create with this same license.

If you have licensing questions or don't understand any of this, feel free to contact me through my
email (on my profile on Github or Gitlab).
